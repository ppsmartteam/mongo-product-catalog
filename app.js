/**
 * Created by Jakk on 12/1/2016 AD.
 */
const express = require('express');
const app = express();

const bodyParser = require('body-parser');
//To be able to put/post/delete request

const mongojs = require('mongojs');
//To map Mongo db for use in JS
const db = mongojs('catalog', ['products']);

//2. Middleware for body-parser
app.use(bodyParser.json());

//1.
app.get('/', function (req, res) {
    res.send('It works!');
});

app.get('/products', function (req, res) {
    //res.send('products work!');
    console.log('Fetching Database');
    db.products.find(function (err, docs) {
        if (err) {
            res.send(err);
        } else {
            console.log('sending products');
            res.json(docs);
        }
    });
});

app.get('/products/:id', function (req, res) {
    db.products.findOne({_id: mongojs.ObjectId(req.params.id)}, function (err, doc) {
        if (err) {
            res.send(err);
        } else {
            console.log('sending product');
            res.json(doc);
        }
    })
});

app.post('/products', function (req, res) {
    db.products.insert(req.body, function (err, doc) {
        if (err) {
            res.send(err);
        } else {
            console.log('adding product');
            res.json(doc);
        }
    })
});

app.put('/products/:id', function (req, res) {
    db.products.findAndModify({
        query: {_id: mongojs.ObjectId(req.params.id)},
        update: {
            $set: {
                name: req.body.name,
                category: req.body.category,
                description: req.body.description
            }
        },
        new: true //if not in existing db -> make new one
    }, function (err, doc) {
        if (err) {
            res.send(err);
        } else {
            console.log('modding product');
            res.json(doc);
        }
    });
});

app.delete('/products/:id', function (req, res) {
    db.products.remove({_id: mongojs.ObjectId(req.params.id)}, function (err, doc) {
        if(err) {
            res.send(err);
        } else {
            res.json(doc);
        }
    })
});

app.listen(3000);
console.log('Server started on port 3000');