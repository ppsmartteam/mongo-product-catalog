db.products.insert({
    name: "Microsoft Surface",
    category: "Electronics",
    model: "7GS-asd03",
    sku: "282983",
    img: "http://appillustrator.com/aha.jpg",
    price: 392.90,
    pickup_available: true,
    description: "askdjfa;skdjfa;ksdjfa;ksdjf;aksjd;fkajsd;kfjas;dkfj",
    release_date: ISODate("2015-09-01"),
    specifications: {
        height: "7.36 inches",
        width: "10.23 inches",
        weight: "0.39 pounds",
        battery_life: "10 hours",
        display_type: "LCD",
        touch_screen: true,
        memory: "2 GB",
        processor_type: "Intel"
    },
    reviews: [
        {
            subject: "Great Tablet",
            body: "bodybodybodybodybodybodybodybodybodybodybodybodybodybodybody",
            rating: 4.9,
            user: "someuser1",
            date: ISODate("2015-11-12"),
            would_recommend: true
        },
        {
            subject: "SUCK DICK Tablet",
            body: "bodybodybodybodybodybodybodybodybodybodybodybodybodybodybody",
            rating: 1.9,
            user: "someuser1",
            date: ISODate("2015-11-12"),
            would_recommend: false
        }
    ],
    protection_services: ['damage', 'battery', 'power']
});

db.products.insert({
    name: "Galaxy",
    category: "Electronics",
    model: "7GSed03",
    sku: "283sks2983",
    img: "http://appillustrator.com/aha.jpg",
    price: 392.90,
    pickup_available: true,
    description: "askdjfa;skdjfa;ksdjfa;ksdjf;aksjd;fkajsd;kfjas;dkfj",
    release_date: ISODate("2015-09-01"),
    specifications: {
        height: "7.36 inches",
        width: "10.23 inches",
        weight: "0.39 pounds",
        battery_life: "10 hours",
        display_type: "LCD",
        touch_screen: true,
        memory: "2 GB",
        processor_type: "Intel"
    },
    reviews: [
        {
            subject: "Great Tablet",
            body: "bodybodybodybodybodybodybodybodybodybodybodybodybodybodybody",
            rating: 4.9,
            user: "someuser1",
            date: ISODate("2015-11-12"),
            would_recommend: true
        },
        {
            subject: "SUCK DICK Tablet",
            body: "bodybodybodybodybodybodybodybodybodybodybodybodybodybodybody",
            rating: 1.9,
            user: "someuser1",
            date: ISODate("2015-11-12"),
            would_recommend: false
        }
    ],
    protection_services: ['damage', 'battery', 'power']
});